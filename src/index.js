const actions = require('./actions');
const middleware = require('./middleware');
const reducer = require('./reducer');
const selectors = require('./selectors');

module.exports = {
  actions: actions,
  middlewareBuilder: middleware,
  modalReducer: reducer,
  modalSelectors: selectors,
  isModalVisible: selectors.isModalVisible,
};
