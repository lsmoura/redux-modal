const actionTypes = require('./action_types');

const SHOW_MODAL = actionTypes.SHOW_MODAL;
const MODAL_CONFIRM = actionTypes.MODAL_CONFIRM;
const MODAL_CANCEL = actionTypes.MODAL_CANCEL;
const HIDE_MODAL = actionTypes.HIDE_MODAL;

function buildMiddleware() {
  const promise_list = {};

  const cleanup = (modal_name) => {
    promise_list[modal_name].accept = null;
    promise_list[modal_name].reject = null;
  };

  return store => next => action => {
    const nextAction = next(action);

    if (!action || !action.__modal) return nextAction;
    if (!action.modal_name) return nextAction;

    const modal_name = action.modal_name;

    if (!promise_list[modal_name]) promise_list[modal_name] = {
      promise: null,
      accept: null,
      reject: null,
      accept_value: null,
      reject_value: null,
    };

    if (action.type === SHOW_MODAL) {
      if (promise_list[modal_name].promise) return promise_list[modal_name].promise;
      promise_list[modal_name].accept_value = null;
      promise_list[modal_name].reject_value = null;
      const returnValue = new Promise((accept, reject) => {
        promise_list[modal_name].accept = (value) => {
          accept(value);
          cleanup(modal_name);
        };
        promise_list[modal_name].reject = (value) => {
          reject(value);
          cleanup(modal_name);
        };

        if (promise_list[modal_name].accept_value) {
          accept(promise_list[modal_name].accept_value);
          cleanup(modal_name);
        } else if (promise_list[modal_name].reject_value) {
          reject(promise_list[modal_name].reject_value);
          cleanup(modal_name);
        }
      });

      promise_list[modal_name].promise = returnValue;

      return returnValue;
    } else if (action.type === MODAL_CONFIRM) {
      promise_list[modal_name].accept_value = action.value;
      if (promise_list[modal_name].accept) promise_list[modal_name].accept(action.value);
    } else if (action.type === MODAL_CANCEL) {
      promise_list[modal_name].reject_value = action.value;
      if (promise_list[modal_name].reject) promise_list[modal_name].reject(action.value);
    } else if (action.type === HIDE_MODAL) {
      if (promise_list[modal_name].reject) promise_list[modal_name].reject('modal_closed');
      promise_list[modal_name].promise = null;
      cleanup(modal_name);
    }

    return nextAction;
  };
};

module.exports = buildMiddleware;
