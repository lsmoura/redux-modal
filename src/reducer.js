const actionTypes = require('./action_types');

const SHOW_MODAL = actionTypes.SHOW_MODAL;
const MODAL_CONFIRM = actionTypes.MODAL_CONFIRM;
const MODAL_CANCEL = actionTypes.MODAL_CANCEL;
const HIDE_MODAL = actionTypes.HIDE_MODAL;

function modals_reducer(state = {}, action) {
  switch (action.type) {
    case SHOW_MODAL:
      if (!action.modal_name) {
        console.error('modal reducer: cannot show modal with no modal name');
      }
      return Object.assign(
        {},
        state,
        {
          [action.modal_name]: Object.assign(
            {},
            state[action.modal_name],
            {
              visible: true,
              modal_state: 'open',
            }
          ),
        }
      );

    case MODAL_CONFIRM:
      if (!action.modal_name) {
        console.error('modal reducer: cannot confirm modal with no modal name');
      }
      return Object.assign(
        {},
        state,
        {
          [action.modal_name]: Object.assign(
            {},
            state[action.modal_name],
            {
              modal_state: 'success',
              confirm_value: action.value,
            }
          ),
        }
      );

    case MODAL_CANCEL:
      if (!action.modal_name) {
        console.error('modal reducer: cannot cancel modal with no modal name');
      }
      return Object.assign(
        {},
        state,
        {
          [action.modal_name]: Object.assign(
            {},
            state[action.modal_name],
            {
              modal_state: 'cancel',
              cancel_value: action.value,
            }
          ),
        }
      );

    case HIDE_MODAL:
      if (!action.modal_name) {
        console.error('modal reducer: cannot hide modal with no modal name');
      }
      return Object.assign(
        {},
        state,
        {
          [action.modal_name]: Object.assign(
            {},
            state[action.modal_name],
            {
              visible: false,
            }
          ),
        }
      );
  }

  return state;
}

module.exports = modals_reducer;
