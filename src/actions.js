const actionTypes = require('./action_types');

const SHOW_MODAL = actionTypes.SHOW_MODAL;
const MODAL_CONFIRM = actionTypes.MODAL_CONFIRM;
const MODAL_CANCEL = actionTypes.MODAL_CANCEL;
const HIDE_MODAL = actionTypes.HIDE_MODAL;

function generateModalActions(modal_name) {
  return {
    show: () => ({ __modal: 1, type: SHOW_MODAL, modal_name }),
    confirm: (confirm_value) => ({ __modal: 1, type: MODAL_CONFIRM, modal_name, value: confirm_value }),
    cancel: (cancel_value) => ({ __modal: 1, type: MODAL_CANCEL, modal_name, value: cancel_value }),
    hide: () => ({ __modal: 1, type: HIDE_MODAL, modal_name }),
  };
};

module.exports = generateModalActions;
