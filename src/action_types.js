const modal_actions_suffix = Math.random().toString(36).substring(7);

module.exports = {
  SHOW_MODAL: 'SHOW_MODAL:' + modal_actions_suffix,
  MODAL_CONFIRM: 'MODAL_CONFIRM:' + modal_actions_suffix,
  MODAL_CANCEL: 'MODAL_CANCEL:' + modal_actions_suffix,
  HIDE_MODAL: 'HIDE_MODAL:' + modal_actions_suffix,
};
