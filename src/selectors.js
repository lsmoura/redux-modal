const isModalVisible = (modal_name) => (state) => {
  try {
    if (!state.modals) return false;
    if (!state.modals[modal_name]) return false;

    return state.modals[modal_name].visible;
  } catch (e) {
    console.warn('cannot determine the visible state of', modal_name, 'from', state);
  }

  return false;
};

module.exports = {
  isModalVisible: isModalVisible,
};
