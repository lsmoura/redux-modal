const assert = require('assert');

const reducer = require('../src/reducer');
const actionCreator = require('../src/actions');

const test_modal_name = 'modal_name' + Math.random().toString(36).substring(7);

describe('reducer', () => {
  let initial_state = null;
  let test_modal_name = 'foo';

  beforeEach(() => {
    initial_state = reducer(undefined, { type: '@@INIT' });
    test_modal_name = 'modal_name' + Math.random().toString(36).substring(7);
  });

  it('creates a proper initial state', () => {
    assert.deepStrictEqual(initial_state, {});
  });

  it('processes show action properly', () => {
    const actions = actionCreator(test_modal_name);
    const next_state = reducer(initial_state, actions.show());
    assert.equal(next_state[test_modal_name].visible, true);
  });

  it('processes hide action properly', () => {
    const actions = actionCreator(test_modal_name);
    const next_state = reducer(initial_state, actions.hide());
    assert.equal(next_state[test_modal_name].visible, false);
  });

  it('processes confirm action properly', () => {
    const actions = actionCreator(test_modal_name);
    const next_state = reducer(initial_state, actions.confirm('boo'));
    assert.equal(next_state[test_modal_name].confirm_value, 'boo');
  });

  it('processes cancel action properly', () => {
    const actions = actionCreator(test_modal_name);
    const next_state = reducer(initial_state, actions.cancel('boo'));
    assert.equal(next_state[test_modal_name].cancel_value, 'boo');
  });
});
