const assert = require('assert');

const actionCreator = require('../src/actions');

const test_modal_name = 'modal_name' + Math.random().toString(36).substring(7);

describe('actions', () => {
  it('exports a function', () => {
    assert.equal(typeof actionCreator, 'function');
  });
  it('has a show action creator', () => {
    const actions = actionCreator('foo');
    assert.equal(typeof actions.show, 'function');
  });
  it('show action has a valid structure', () => {
    const actions = actionCreator(test_modal_name);
    const show = actions.show();
    assert.equal(show.__modal, 1);
    assert.equal(show.modal_name, test_modal_name);
    assert.equal(typeof show.type, 'string');
    assert.ok(show.type.length > 0);
  });
  it('has a confirm action creator', () => {
    const actions = actionCreator('foo');
    assert.equal(typeof actions.confirm, 'function');
  });
  it('confirm action has a valid structure', () => {
    const actions = actionCreator(test_modal_name);
    const confirm = actions.confirm('boo');
    assert.equal(confirm.__modal, 1);
    assert.equal(confirm.modal_name, test_modal_name);
    assert.equal(confirm.value, 'boo');
    assert.equal(typeof confirm.type, 'string');
    assert.ok(confirm.type.length > 0);
  });
  it('has a cancel action creator', () => {
    const actions = actionCreator('foo');
    assert.equal(typeof actions.cancel, 'function');
  });
  it('cancel action has a valid structure', () => {
    const actions = actionCreator(test_modal_name);
    const cancel = actions.cancel();
    assert.equal(cancel.__modal, 1);
    assert.equal(cancel.modal_name, test_modal_name);
    assert.equal(typeof cancel.type, 'string');
    assert.ok(cancel.type.length > 0);
  });
  it('has a hide action creator', () => {
    const actions = actionCreator('foo');
    assert.equal(typeof actions.hide, 'function');
  });
  it('hide action has a valid structure', () => {
    const actions = actionCreator(test_modal_name);
    const hide = actions.hide();
    assert.equal(hide.__modal, 1);
    assert.equal(hide.modal_name, test_modal_name);
    assert.equal(typeof hide.type, 'string');
    assert.ok(hide.type.length > 0);
  });
});
